# Load the Pandas libraries with alias 'pd' 
import pandas as pd 
from tldextract import extract
import numpy as np
dns_data = pd.read_csv("trace-anonyme-dns-138.48.4.4.csv")		#Put the extracted dns data file returned by dns_extractor

# Preview the first 5 lines of the loaded dns_data 
#print(dns_data.head())

print("########################################")

with open("whitelist.csv") as f:
    whitelist = f.readlines()
whitelist = [x.strip() for x in whitelist] 

with open("blacklist.csv") as f:
    blacklist = f.readlines()
blacklist = [x.strip() for x in blacklist] 

print("########################################")

dns_extracted = pd.DataFrame(dns_data, columns= ["ts_sec","ts_usec","identifier","opcode","aa_flag","tc_flag","rd_flag","ra_flag","rcode","questions_count","answers_count","authority_count","additional_count","q_name","q_type","q_class"])#Choose the field you want
#dns_extracted["ID"]="-1"


df2 = pd.DataFrame(columns= ["ts_sec","ts_usec","identifier","opcode","aa_flag","tc_flag","rd_flag","ra_flag","rcode","questions_count","answers_count","authority_count","additional_count","q_name","q_type","q_class"])

to_be_modified = []

for i in range(len(dns_extracted)):
	domain=dns_extracted.loc[i,'q_name']
	if(pd.notna(domain)):
		tsd, td, tsu = extract(domain)
		domain_parsed=td+"."+tsu
		if domain_parsed in blacklist:
			#print(dns_extracted.loc[i].to_string())
			#dns_extracted.loc[i,'ID'] = 0
			#df2 = df2.append(dns_extracted.loc[i])
			print(domain+" | "+domain_parsed+" | B")
	if(i%1000==0):
		print(i)		

print("########################################")

#df2["ID"]="0"


#for i in range(len(to_be_modified)):
#	dns_extracted.loc[to_be_modified[i],'ID'] = 0
#	if(i%100==0):
#		print(i)

#print("modified")


#df2=df2.append(dns_extracted[dns_extracted['ID'] == 0])
#df2.to_csv("labelised_white")

"""
for domain in dns_extracted:
	for whitelist_domain in whitelist:
		if(whitelist_domain in domain):
			print("WL: "+whitelist_domain+"		DN: "+domain)
"""

#dns_date_extracted.to_csv(r'/home/kali/Desktop/Project3/Keep_Interested_field.csv', index = False, header=True)

#print(dns_date_extracted.head())
